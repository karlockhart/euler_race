package main

import (
	"fmt"
	"math"
	"time"
)

type PaliBits struct {
	len   int
	bits  []bool
	nTrue int
	it    int
}

// NewPaliBits returns a new palindrome generator
func NewPaliBits() *PaliBits {
	p := PaliBits{len: 1, nTrue: 1, bits: make([]bool, 1)}
	p.bits[0] = true
	return &p
}

// Next returns the next bit palindrome
func (p *PaliBits) Next() {
	// If all are true, next palindrome base
	if p.len <= p.nTrue {
		p.len++
		p.bits = make([]bool, p.len)
		p.bits[p.len-1] = true
		p.bits[0] = true
		p.nTrue = 2
		p.it = 1
		return
	}

	// If item at iterator is true just reset
	if p.bits[p.len-1-p.it] == true {
		fmt.Println("Reset")
		p.it = 1
		return
	}

	// If considering cdc element(s) set to true and set nTrue
	if p.len-1-p.it == p.len/2 {
		if p.len%2 != 0 {
			p.bits[p.len-1-p.it] = true
			p.nTrue++
			if p.len > 3 {
				p.bits[p.len-p.it] = false
				p.bits[p.it-1] = false
				p.nTrue = p.nTrue - 2
				p.it = 1
			}
			return
		}
	}

	// If element at iterator isn't set and left is set and not the base move
	if p.bits[p.len-1-p.it] == false {
		if p.it > 1 {
			p.bits[p.len-p.it] = false
			p.bits[p.it-1] = false
		} else {
			p.nTrue += 2
		}
		p.bits[p.len-1-p.it] = true
		p.bits[p.it] = true

		p.it++
		return
	}
}

// Print the integer (debug)
func (p *PaliBits) Print() {
	fmt.Println(p.bits, p.nTrue, p.it)
	for _, n := range p.bits {
		if n == true {
			fmt.Print("1")
		} else {
			fmt.Print("0")
		}
	}
	fmt.Println()
}

// AsInt64 returns this value as int 64
func (p *PaliBits) AsInt64() int64 {
	i := 0.0
	for n, v := range p.bits {
		if v == true {
			i += math.Pow(float64(2), float64(n))
		}
	}
	return int64(i)
}

// IsPalindrome returns true if val is a palindrome
func IsPalindrome(val int64) bool {
	nDig := int(math.Log10(float64(val)) + 1)
	for i := 0; i <= nDig-1-i; i++ {
		if (val/int64(math.Pow(10.0, float64(i))))%10 != (val/int64(math.Pow(10.0, float64(nDig-1-i))))%10 {
			return false
		}
	}
	return true

}

func main() {

	start := time.Now()
	p := NewPaliBits()
	for p.AsInt64() < 1000 {
		p.Print()
		p.Next()
	}
	end := time.Now()
	fmt.Println("elapsed(ns):", end.Nanosecond()-start.Nanosecond())
}

func testmain() {

	start := time.Now()
	p := NewPaliBits()
	accum := int64(0)
	for p.AsInt64() < 1000000 {
		curr := p.AsInt64()
		fmt.Println(curr)
		if IsPalindrome(curr) {
			fmt.Println(curr)
			accum += curr
		}
		p.Next()
		curr = p.AsInt64()
	}
	fmt.Println(accum)
	end := time.Now()
	fmt.Println("elapsed(ns):", end.Nanosecond()-start.Nanosecond())
}
