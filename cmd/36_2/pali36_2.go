package main

import (
	"fmt"
	"math"
	"time"
)

type PaliBit struct {
	Prev  *PaliBit
	Next  *PaliBit
	Value int
	Mul   int
}

type PaliBits struct {
	MSB    *PaliBit
	LSB    *PaliBit
	Head   *PaliBit
	Tail   *PaliBit
	Active int
	Count  int
}

// NewPaliBits returns a new palindrome generator
func NewPaliBits() *PaliBits {
	b := PaliBit{Value: 1, Mul: 1, Prev: nil, Next: nil}
	p := PaliBits{MSB: &b, LSB: &b, Head: &b, Tail: &b, Active: 1, Count: 1}
	return &p
}

// Next returns the next bit palindrome
func (p *PaliBits) Next() {
	// If the bits are all start over
	if p.GetActiveCount() == p.Count {
		p.Add()
		return
	}

	if p.Head.Prev == p.Tail || p.Tail.Next == p.Head || p.Head.Prev.Mul == 1 || p.Tail.Next.Mul == 1 {
		p.Head = p.MSB
		p.Tail = p.LSB
	}

	if p.Head.Prev != nil && p.Head.Prev.Mul == 0 {
		p.Head.Prev.Mul = 1
		if p.Head != p.MSB {
			p.Head.Mul = 0
		}
		p.Head = p.Head.Prev
	}

	if p.Tail.Next != nil && p.Tail.Next.Mul == 0 {
		p.Tail.Next.Mul = 1
		if p.Tail != p.LSB {
			p.Tail.Mul = 0
		}
		p.Tail = p.Tail.Next
	} else if p.Tail.Next == p.Head && p.Tail != p.LSB {
		p.Tail.Mul = 0
		p.Tail = p.LSB
		p.Head = p.MSB
	}
}

func (p *PaliBits) Add() {
	// Create a new PaliBit
	b := PaliBit{
		Value: int(math.Pow(2.0, float64(p.Count))),
		Mul:   1,
		Prev:  p.MSB,
	}

	// Set the Old MSB to point to the New Palibit
	p.MSB.Next = &b

	// Point the MSB at the new PaliBit
	p.MSB = &b

	p.Head = p.MSB
	p.Tail = p.LSB
	p.Active = 2
	p.Count++

	// Reset the PaliBit to a Palindrome Base
	n := p.LSB
	for {
		if n != p.LSB && n != p.MSB {
			n.Mul = 0
		}
		if n.Next != nil {
			n = n.Next
		} else {
			break
		}
	}

}

// Print the integer (debug)
func (p *PaliBits) Print() {
	n := p.LSB
	for {
		fmt.Print(n.Mul)
		if n.Next != nil {
			n = n.Next
		} else {
			break
		}
	}
	fmt.Println()
}

func (p *PaliBits) GetActiveCount() int {
	i := 0
	n := p.LSB
	for {
		i += n.Mul
		if n.Next != nil {
			n = n.Next
		} else {
			break
		}
	}
	return i
}

// AsInt64 returns this value as int 64
func (p *PaliBits) AsInt() int {
	i := 0
	n := p.LSB
	for {
		i += n.Mul * n.Value
		if n.Next != nil {
			n = n.Next
		} else {
			break
		}
	}
	return i
}

// IsPalindrome returns true if val is a palindrome
func IsPalindrome(val int) bool {
	nDig := int(math.Log10(float64(val)) + 1)
	for i := 0; i <= nDig-1-i; i++ {
		if (val/int(math.Pow(10.0, float64(i))))%10 != (val/int(math.Pow(10.0, float64(nDig-1-i))))%10 {
			return false
		}
	}
	return true

}

func main() {

	start := time.Now()
	p := NewPaliBits()
	accum := 0
	for p.GetActiveCount() <= 20 {
		fmt.Print(p.AsInt(), ", ")
		p.Print()

		curr := p.AsInt()
		if IsPalindrome(curr) && curr <= 1000000 {
			fmt.Println("Match:", curr)
			accum += curr
		}
		p.Next()
	}
	fmt.Println("ANS:", accum)
	end := time.Now()
	fmt.Println("elapsed(ns):", end.Nanosecond()-start.Nanosecond())
}
